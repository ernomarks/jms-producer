package nl.logius.osdbk.jms.factory;

import nl.clockwork.ebms.DataSource;
import nl.clockwork.ebms.MessageProperties;
import nl.logius.osdbk.util.EbMSBrokerUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.TextMessage;

@Component
public class MessageFactory {

    private Logger logger = LoggerFactory.getLogger(MessageFactory.class);

    public TextMessage createTextMessage(Session session, nl.clockwork.ebms.Message message) throws JMSException {
        logger.info("JMSProducer:MessageFactory:createTextMessage using xml");
        MessageProperties messageContext = message.getProperties();
        DataSource datasource = message.getDataSource().get(0);

        TextMessage textMessage = session.createTextMessage(new String(datasource.getContent()));
        setJMSHeaders(textMessage, messageContext);

        return textMessage;
    }

     private void setJMSHeaders(TextMessage message, MessageProperties messageProperties) throws JMSException {
        message.setStringProperty("x_aux_action", messageProperties.getAction());
        message.setStringProperty("x_aux_activity", EbMSBrokerUtils.stripUrnPrefixes(messageProperties.getService()));
        message.setStringProperty("x_aux_process_type", EbMSBrokerUtils.stripUrnPrefixes(messageProperties.getService()));
        message.setStringProperty("x_aux_process_version", "1.0");
        message.setStringProperty("x_aux_production", "Production");
        message.setStringProperty("x_aux_protocol", "ebMS");
        message.setStringProperty("x_aux_protocol_version", "2.0");
        message.setStringProperty("x_aux_system_msg_id", messageProperties.getMessageId());
        message.setStringProperty("x_aux_receiver_id", EbMSBrokerUtils.stripUrnPrefixes(messageProperties.getToParty().getPartyId()));
        message.setStringProperty("x_aux_sender_id", EbMSBrokerUtils.stripUrnPrefixes(messageProperties.getFromParty().getPartyId()));
        message.setStringProperty("x_aux_process_instance_id", messageProperties.getConversationId());
        message.setStringProperty("x_aux_seq_number", "0");
        message.setStringProperty("x_aux_msg_order", "false");
    }

}
