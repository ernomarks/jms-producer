package nl.logius.osdbk.jms.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "jms-producer.configuration")
public class JMSProducerConfiguration {
    private List<Route> routes;

    public static class Route {
        private String oin;
        private String cpaId;
        private String service;
        private String action;
        private String queueName;

        public String getOin() {
            return oin;
        }

        public void setOin(String oin) {
            this.oin = oin;
        }

        public String getCpaId() {
            return cpaId;
        }

        public void setCpaId(String cpaId) {
            this.cpaId = cpaId;
        }

        public String getService() {
            return service;
        }

        public void setService(String service) {
            this.service = service;
        }

        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }

        public String getQueueName() {
            return queueName;
        }

        public void setQueueName(String queueName) {
            this.queueName = queueName;
        }

    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }
}
