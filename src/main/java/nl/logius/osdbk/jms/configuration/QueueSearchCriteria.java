package nl.logius.osdbk.jms.configuration;

public class QueueSearchCriteria {
    private String oin;
    private String cpaId;
    private String service;
    private String action;

    public QueueSearchCriteria(String oin, String cpaId, String service, String action) {
        this.oin = oin;
        this.cpaId = cpaId;
        this.service = service;
        this.action = action;
    }

    public String getOin() {
        return oin;
    }

    public String getCpaId() {
        return cpaId;
    }

    public String getService() {
        return service;
    }

    public String getAction() {
        return action;
    }

}
