package nl.logius.osdbk.jms;

import com.sun.xml.ws.client.ClientTransportException;
import nl.clockwork.ebms.EbMSMessageService;
import nl.clockwork.ebms.EbMSMessageServiceException_Exception;
import nl.clockwork.ebms.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.jms.JMSException;

@Component
@ConditionalOnProperty(value="jms-producer.enabled", havingValue = "true", matchIfMissing = true)
public class ReceivedEventListener {
    private Logger logger = LoggerFactory.getLogger(ReceivedEventListener.class);

    @Autowired
    private EbMSMessageService messageService;
    @Autowired
    private JmsMessageProducer messageProducer;

    @Retryable(
            value = ClientTransportException.class, // infinite retry when ebMS Core endpoint is unavailable
            maxAttempts = Integer.MAX_VALUE,
            backoff = @Backoff(delay = 1000, maxDelay = 30000),
            listeners = "jmsRetryListener"
    )
    @JmsListener(destination = "${event.queue.received.name}", concurrency = "${event.queue.received.concurrency}", containerFactory = "jmsListenerContainerFactory")
    @Transactional(propagation = Propagation.REQUIRED)
    public void onMessage(javax.jms.Message jmsMessage) throws JMSException, EbMSMessageServiceException_Exception {
        String messageId = jmsMessage.getStringProperty("messageId");
        logger.debug(String.format("Received event for message %s", messageId));

        Message message = messageService.getMessage(messageId, true);
        if(message == null) {
            throw new JMSException(String.format("Message with ID %s does not exists.", messageId));
        }

        messageProducer.sendMessage(message);
    }
}