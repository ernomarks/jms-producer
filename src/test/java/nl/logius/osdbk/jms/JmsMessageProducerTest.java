package nl.logius.osdbk.jms;

import nl.clockwork.ebms.DataSource;
import nl.clockwork.ebms.Message;
import nl.clockwork.ebms.MessageProperties;
import nl.clockwork.ebms.Party;
import nl.logius.osdbk.jms.configuration.QueueSearchCriteria;
import nl.logius.osdbk.jms.configuration.QueueService;
import nl.logius.osdbk.jms.exception.EventException;
import nl.logius.osdbk.jms.exception.QueueNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class JmsMessageProducerTest {

    @Mock
    private QueueService queueService;

    @Mock
    private JmsTemplate jmsTemplate;

    @InjectMocks
    private JmsMessageProducer messageProducer;

    @Mock
    private Message message;

    @Mock
    private MessageProperties messageProperties;

    @Mock
    private List<DataSource> dataSources;

    @BeforeEach
    public void setup() {
        when(message.getProperties()).thenReturn(messageProperties);

        Party fromParty = mock(Party.class);
        when(messageProperties.getFromParty()).thenReturn(fromParty);
        when(fromParty.getPartyId()).thenReturn("12345");

        when(messageProperties.getCpaId()).thenReturn("cpa-id");
        when(messageProperties.getService()).thenReturn("service");
        when(messageProperties.getAction()).thenReturn("verstrekkingDoorLV");
    }

    @Test
    void sendMessage() {
        when(queueService.getQueueName(any(QueueSearchCriteria.class))).thenReturn("verstrekkingenInQueue");
        when(message.getDataSource()).thenReturn(dataSources);
        when(dataSources.isEmpty()).thenReturn(false);
        Party toParty = mock(Party.class);
        when(messageProperties.getToParty()).thenReturn(toParty);
        when(toParty.getPartyId()).thenReturn("54321");

        messageProducer.sendMessage(message);
        verify(jmsTemplate, times(1)).send(eq("verstrekkingenInQueue"), any(MessageCreator.class));
    }

    @Test
    void sendMessageQueueNotFound() {
        when(queueService.getQueueName(any(QueueSearchCriteria.class))).thenThrow(new QueueNotFoundException(""));

        Assertions.assertThrows(QueueNotFoundException.class, () -> {
            messageProducer.sendMessage(message);
        });
    }

    @Test
    void sendMessageWithDatasourcesIsNull() {
        when(queueService.getQueueName(any(QueueSearchCriteria.class))).thenReturn("verstrekkingenInQueue");
        when(message.getDataSource()).thenReturn(null);
        Party toParty = mock(Party.class);
        when(messageProperties.getToParty()).thenReturn(toParty);
        when(toParty.getPartyId()).thenReturn("54321");

        Assertions.assertThrows(EventException.class, () -> {
            messageProducer.sendMessage(message);
        });
    }

    @Test
    void sendMessageWithDatasourcesIsEmpty() {
        when(queueService.getQueueName(any(QueueSearchCriteria.class))).thenReturn("verstrekkingenInQueue");
        when(message.getDataSource()).thenReturn(dataSources);
        when(dataSources.isEmpty()).thenReturn(true);
        Party toParty = mock(Party.class);
        when(messageProperties.getToParty()).thenReturn(toParty);
        when(toParty.getPartyId()).thenReturn("54321");

        Assertions.assertThrows(EventException.class, () -> {
            messageProducer.sendMessage(message);
        });
    }

}