package nl.logius.osdbk.jms.exception;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class QueueNotFoundExceptionTest {

    @Test
    public void queueNotFoundException(){

        Throwable exception = assertThrows(QueueNotFoundException.class, () -> {
            throw new QueueNotFoundException("test");
        }  );

        assertEquals("test", exception.getMessage());

    }
}