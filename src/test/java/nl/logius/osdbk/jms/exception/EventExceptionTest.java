package nl.logius.osdbk.jms.exception;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class EventExceptionTest {

    @Test
    public void eventException(){

        Throwable exception = assertThrows(EventException.class, () -> {
            throw new EventException("test");
        }  );

        assertEquals("test", exception.getMessage());

    }

}