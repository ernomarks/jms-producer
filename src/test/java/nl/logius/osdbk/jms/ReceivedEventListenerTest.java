package nl.logius.osdbk.jms;

import nl.clockwork.ebms.EbMSMessageService;
import nl.clockwork.ebms.EbMSMessageServiceException_Exception;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.jms.JMSException;
import javax.jms.Message;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ReceivedEventListenerTest {

    @Mock
    private EbMSMessageService messageService;
    @Mock
    private JmsMessageProducer messageProducer;
    @InjectMocks
    private ReceivedEventListener eventListener;

    @Mock
    private Message message;

    @Mock
    private nl.clockwork.ebms.Message content;

    @BeforeEach
    public void setup() throws JMSException, EbMSMessageServiceException_Exception {
        when(message.getStringProperty("messageId")).thenReturn("12345");
        when(messageService.getMessage("12345", true)).thenReturn(content);
    }

    @Test
    void onMessage() throws JMSException, EbMSMessageServiceException_Exception {
        eventListener.onMessage(message);
        verify(messageProducer, times(1)).sendMessage(content);
    }

    @Test
    void onMessageWithMessageNotFound() throws EbMSMessageServiceException_Exception {
        when(messageService.getMessage("12345", true)).thenReturn(null);

        Assertions.assertThrows(JMSException.class, () -> {
            eventListener.onMessage(message);
        });
    }

}