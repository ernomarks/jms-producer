package nl.logius.osdbk.jms.factory;

import nl.clockwork.ebms.DataSource;
import nl.clockwork.ebms.MessageProperties;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.activation.DataHandler;
import javax.jms.*;
import java.io.IOException;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MessageFactoryTest {

    @InjectMocks
    private MessageFactory messageFactory;

    @Test
    void createTextMessage() throws JMSException, IOException {
        Session session = mock(Session.class);
        nl.clockwork.ebms.Message ebmsMessage = mock(nl.clockwork.ebms.Message.class);
        DataSource dataSource = mock(DataSource.class);
        MessageProperties messageProperties = mock(MessageProperties.class, Mockito.RETURNS_DEEP_STUBS);
        DataHandler attachment = mock(DataHandler.class);
        when(ebmsMessage.getDataSource()).thenReturn(Arrays.asList(dataSource));
        when(dataSource.getContent()).thenReturn("mock-ebmsMessage".getBytes());
        when(session.createTextMessage("mock-ebmsMessage")).thenReturn(createEmptyMessage());
        when(ebmsMessage.getProperties()).thenReturn(messageProperties);

        when(messageProperties.getAction()).thenReturn("verstrekkingDoorLV");
        when(messageProperties.getService()).thenReturn("dgl:ontvangen:1.0");
        when(messageProperties.getMessageId()).thenReturn("message-id");
        when(messageProperties.getToParty().getPartyId()).thenReturn("12345");
        when(messageProperties.getFromParty().getPartyId()).thenReturn("54321");
        when(messageProperties.getConversationId()).thenReturn("conversation-id");

        Message message = messageFactory.createTextMessage(session, ebmsMessage);
        assertEquals("message-id", message.getStringProperty("x_aux_system_msg_id"));
        assertEquals("verstrekkingDoorLV", message.getStringProperty("x_aux_action"));
        assertEquals("dgl:ontvangen:1.0", message.getStringProperty("x_aux_activity"));
        assertEquals("dgl:ontvangen:1.0", message.getStringProperty("x_aux_process_type"));
        message.setStringProperty("12345", message.getStringProperty("x_aux_sender_id"));
        message.setStringProperty("54321", message.getStringProperty("x_aux_receiver_id"));
        message.setStringProperty("conversation-id", message.getStringProperty("x_aux_process_instance_id"));
    }

    private TextMessage createEmptyMessage() throws JMSException {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("vm://localhost?broker.persistent=false");
        Session session = connectionFactory.createConnection().createSession(true, 0);
        return session.createTextMessage();
    }

}